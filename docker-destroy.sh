#! /bin/bash

# stop all docker containers:
containers=$(docker container ls -q)
if [[ -n $containers ]]; then
    docker container stop $containers
fi

# remove all docker containers:
containers=$(docker container ls -qa)
if [[ -n $containers ]]; then
    docker container rm $containers
fi
