#! /bin/env python3

import requests

def main():
    r = requests.get('http://localhost:8200/api/search', params={"q": "brown"})
    if r.status_code == 200:
        j = r.json()
        print(len(j))
        r2 = requests.get('http://localhost:8200/api/summary', params={"url": j[0].get('link', "")})
        if r2.status_code == 200:
            j2 = r2.json()
            print(j2)

if __name__ == "__main__":
    main()