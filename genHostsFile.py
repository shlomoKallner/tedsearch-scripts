#! /bin/env python3

# for usage run this script with the "-h" or "--help" flags.

import argparse
import argcomplete
# import requests
import yaml
import json
import io
import sys
from pathlib import Path
from jinja2 import Environment, PackageLoader, FileSystemLoader, select_autoescape

from utils import csvkv, dbg


def version() -> str:
    return "0.1.0"

def parse_group_file(infile: io.TextIOWrapper, debugger: ("io.TextIOWrapper | None") = None) -> ("dict< str, dict< str, str > >"):
    groups = {}
    dbg.printer(debugger, "in function parse_group_file")
    for line in infile:
        dbg.printer(debugger, line)
        l1 = line.strip()
        dbg.printer(debugger, l1)
        if len(l1) > 0:
            rec = csvkv.SimpleLine(line, required_keys=['group', 'public_ip', 'private_ip'])
            dbg.printer(debugger, rec)
            for i in rec.get('group', []):
                dbg.printer(debugger, i)
                if i not in groups:
                    groups[i] = {}
                for j in rec.get('public_ip', []):
                    dbg.printer(debugger, j)
                    if j not in groups[i].keys():
                        groups[i][j] = {}
                        k = rec.get('private_ip', [])
                        groups[i][j]['private_ip'] = k[0]
    dbg.printer(debugger, groups)
    dbg.printer(debugger, "exiting function parse_group_file")
    return groups

def convert_host_groups_to_yaml(groups: dict) -> dict :
    yd = {'all': {}}
    for i in groups.keys():
        if i == '' and len(groups[i]) > 0:
            gh = {}
            for j in groups[i].keys():
                if j not in gh:
                    gh[j] = {}
                for k, v in groups[i][j].items():
                    gh[j][k] = v
            if len(gh) > 0:
                yd['all']['hosts'] = gh
        elif len(groups[i]) > 0:
            if 'children' not in yd['all']:
                yd['all']['children'] = {}
            if i not in yd['all']['children']:
                gh = {'hosts': {}}
                for j in groups[i].keys():
                    if j not in gh['hosts']:
                        gh['hosts'][j] = {}
                    for k, v in groups[i][j].items():
                        gh['hosts'][j][k] = v
                if len(gh['hosts']) > 0:
                    yd['all']['children'][i] = gh
    return yd

def parse_json_group_file(infile: io.TextIOWrapper, debugger: ("io.TextIOWrapper | None") = None) -> ("dict< str, dict< str, str > >"):
    groups = {}
    dbg.printer(debugger, "in function parse_json_group_file")
    jf = json.load(infile)
    dbg.printer(debugger, jf)
    for rec in jf['host_groups']['value']:
        dbg.printer(debugger, rec)
        if rec['group'] not in groups:
            groups[rec['group']] = {}
        for host in rec['hosts']:
            if host['public_ip'] not in groups[rec['group']]:
                groups[rec['group']][host['public_ip']] = {}
            if host['private_ip'] not in groups[rec['group']][host['public_ip']]:
                groups[rec['group']][host['public_ip']]['private_ip'] = host['private_ip']
            for k, v in host['extra_vars'].items():
                if k not in groups[rec['group']][host['public_ip']]:
                    groups[rec['group']][host['public_ip']][k] = v
    dbg.printer(debugger, groups)
    dbg.printer(debugger, "exiting function parse_json_group_file")
    return groups
    
# def convert_json_group_file_to_yaml(groups: dict) -> dict :


def main(infile: io.TextIOWrapper, outfile: io.TextIOWrapper, ver=1, debugger = None):
    if ver == 2:
        groups = parse_json_group_file(infile, debugger)
    else:
        groups = parse_group_file(infile, debugger)
    yaml.dump(
        convert_host_groups_to_yaml(groups), outfile, 
        default_flow_style=False, explicit_start=True
    )

def main2(infile: io.TextIOWrapper, outfile: io.TextIOWrapper, ver=1, debugger = None):
    dbg.printer(debugger, str(Path.cwd()))
    env = Environment(
        loader=FileSystemLoader(str(Path.cwd().joinpath('template'))),
        autoescape=select_autoescape(['html', 'xml', 'yaml', 'yml'])
    )
    if ver == 2:
        groups = parse_json_group_file(infile, debugger)
    else:
        groups = parse_group_file(infile, debugger)
    template = env.get_template('ansible_host.yml.j2')
    outp = template.render(groups=groups)
    dbg.printer(debugger, outp)
    outfile.write(outp)



if __name__ == "__main__":
    # input a text file of host mappings records
    #   using the format of:
    #       - one record per line
    #       - each line has comma-separated key/value pairs
    #       - each key/value pair is formatted:
    #           "<key>: <value>"
    #   For now the "group" and "ip" keys are _REQUIRED_
    #       but more may be added.
    arg_parser = argparse.ArgumentParser(
        description="Converts our key:value <hosts>.txt files to <hosts>.yml files for Ansible."
    )
    arg_parser.add_argument(
        'input', type=argparse.FileType('rt'), # nargs='?',
        metavar="INPUT_FILE" #, default=sys.stdin
    )
    arg_parser.add_argument(
        'output', type=argparse.FileType('wt'), # nargs='?',
        metavar="OUTPUT_FILE" #, default=sys.stdout
    )
    arg_parser.add_argument(
        '-d', '--debug', action='store_true'
    )
    # arg_parser.add_argument(
    #     '-j', '--jinja', action='store_true'
    # )
    arg_parser.add_argument(
        '-t', '--useTerraformOutput', action='store_true'
    )
    argcomplete.autocomplete(arg_parser)
    args = arg_parser.parse_args()
    groups = {}
    # for line in the input file:
    #   split the line by sep (default: ',') and extract data
    #   check groups if a given group_id exists
    #       if not, create it
    #   check if a given ip is in the given group
    #       if not, append it
    with args.input as infile, args.output as outfile:
        # if args.jinja:
        #     main2(infile, outfile, sys.stdout if args.debug else None)
        # else:
        main(infile, outfile, 2 if args.useTerraformOutput else 1 , sys.stdout if args.debug else None)