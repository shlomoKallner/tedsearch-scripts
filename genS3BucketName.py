#! /bin/env python3

import string
import secrets

def gen(len=4):
    MAX_LEN = 63
    MIN_LEN = 4
    res = []
    rnd = secrets.SystemRandom()
    res.append(secrets.choice(string.ascii_lowercase))
    for i in range(1, len-1):
        res.append(secrets.choice(str().join([string.ascii_lowercase, string.digits])))
    res.append(secrets.choice(string.ascii_lowercase))
    return str().join(res)

def get_my_str():
    return "com.develeap.devops-course.user.shlomo-kallner.terraform"

if __name__ == "__main__":
    for i in range(1, 11):
        print(gen(i*6))
    my_str = get_my_str()
    print(my_str, len(my_str))