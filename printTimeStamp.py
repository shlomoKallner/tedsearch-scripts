#! /bin/env python3

from datetime import datetime, tzinfo

def genTimeStamp(format_str="%d-%b-%Y_%H-%M-%S", tz=None):
    dt = datetime.now(tz)
    return dt.strftime(format_str) 

if __name__ == "__main__":
    print(genTimeStamp())