#! /bin/env python3

import io
import sys

# NativeEnvironment is used by the Scripted Classes
from jinja2.nativetypes import NativeEnvironment

class SimpleLine(dict):
    def __init__(
        self, line : str, delimiter=",", 
        separator=":", required_keys=[],
        field_comment_starters=[]
        ):
            # , escape_char="\\" ... future feature
            # Note: empty ('default') keys are now implemented as well as commented out fields
            tmp_dict = {}
            tmp_l = line.strip()
            if len(tmp_l) > 0:
                tmp = tmp_l.split(delimiter)
                for i in tmp:
                    kv = i.strip().split(separator)
                    if len(kv) >= 2:
                        key = kv[0].strip()
                        for c in field_comment_starters:
                            if isinstance(c, str):
                                t_fn = key.find(c)
                                if t_fn == 0:
                                    key = ''
                                elif t_fn > 0:
                                    key = key[0:t_fn]
                            else:
                                raise TypeError("Only Strings can be used as Comment Starters!")
                        if key not in tmp_dict:
                            tmp_dict[key] = []
                        for n in range(1, len(kv)):
                            t_str = kv[n].strip()
                            for c in field_comment_starters:
                                if isinstance(c, str):
                                    t_fn = t_str.find(c)
                                    if t_fn == 0:
                                        # the field-value has been commented out..
                                        t_str = ''
                                    elif t_fn > 0:
                                        # part of the field-value has commented out..
                                        t_str = t_str[0:t_fn]
                                else:
                                    raise TypeError("Only Strings can be used as Comment Starters!")
                            if len(t_str) > 0:
                                tmp_dict[key].append(t_str)
            for j in required_keys:
                if not isinstance(j, str) or len(j) == 0:
                    raise TypeError("required_keys Must be a sequence of non-Empty Strings!")
                elif j not in tmp_dict:
                    raise ValueError("Field {} is REQUIRED!".format(j))
            super().__init__(tmp_dict)

class SimpleFile(list):
    def __init__(
        self, file_object: io.TextIOWrapper, 
        line_comment_starters=['#'], 
        kv_set_delimiter=",", 
        key_value_field_separator=":", required_keys=[],
        field_comment_starters=[]
    ):
        res = []
        if file_object.readable():
            for line in file_object:
                l1 = line.strip()
                for c in line_comment_starters:
                    if isinstance(c, str):
                        t_fnd = l1.find(c)
                        if t_fnd == 0:
                            l1 = ''
                    else:
                        raise TypeError("Only Strings can be Line Comment Starters!")
                if len(l1) > 0:
                    try:
                        t_lst = csvkv(
                            l1, kv_set_delimiter, key_value_field_separator,
                            required_keys, field_comment_starters
                        )
                        res.append(t_lst)
                    except ValueError as ev:
                        raise
                    except TypeError as et:
                        raise
            super().__init__(res)
        else:
            raise ValueError("The Input File Must be Readable!")

##############################################################
#### The Scripted Classes below are for a DIFFERENT TIME! ####
#### (they are not tested nor necessarily feature complete) ##
##############################################################

class SimpleScriptedValueList():
    # '''
    #     A Simple Script is a condition, that if True "returns" the value
    #         if it's False, "blocks" the value.
    #     An empty (non-existent) condition is ALWAYS True.
    #     The condition may contain variables to check..
    #     Uses Jinj2.NativeEnvironment to evaluate.
    #     Should refer to other values on the "record" typically of other "keys".
    #     Although the implementation allows for mutiple "scripts", 
    #     in practice will ignore any other scripts except the first.
    # '''
    def __init__(self, vals: "list<str>", script_starter="?"):
        self._list = []
        self.extend(vals, script_starter)

    def append(self, val:str, script_starter="?"):
        if isinstance(val, str):
            v = val.strip()
            if len(v) > 0:
                vl = v.split(script_starter)
                if len(vl) > 1:
                    self._list.append({'val': vl[0], 'script': vl[1]})
                elif len(vl) == 1:
                    self._list.append({'val': vl[0], 'script': None})

    def extend(self, vals: "seq<str>", script_starter="?"):
        if isinstance(vals, (list, tuple)):
            for i in range(0, len(vals)):
                self.append(vals[i], script_starter)

    def all(self, **kwargs):
        res = []
        for i in range(0, len(self._dict)):
            val = self.get(i, **kwargs)
            if val is not None:
                res.append(val)
        return res

    def get(self, index=-1, **kwargs):
        if isinstance(index, int):
            if index >= 0 and index < len(self._dict):
                vs = self._list[index]
                if isinstance(vs, dict):
                    val = vs['val']
                    scrpt = vs['script']
                else:
                    val = None
                    scrpt = None
                if val is not None:
                    if scrpt is None:
                        return val
                    elif self._eval(scrpt, **kwargs):
                        return val
        return None
    
    def _eval(self, script:str, **kwargs) -> bool:
        env = NativeEnvironment()
        t = env.from_string(str(' ').join(['{{', script, '}}']))
        res = t.render(**kwargs)
        return bool(res)


class SimpleScriptedLine():
    def __init__(self,  line : str, delimiter=",", 
        separator=":", required_keys=[],
        field_comment_starters=[], script_starter="?"
        ):
            self._dict = {}
            # self._script_starter = script_starter
            tmp_dict = SimpleLine(
                line, delimiter, separator, 
                required_keys, field_comment_starters
            )
            for key, val in tmp_dict.items():
                if key not in self._dict:
                    self._dict[key] = SimpleScriptedValueList(val, script_starter)
                else:
                    self._dict[key].extend(val, script_starter)   

    def get(self, key:str, **kwargs)-> "list<str>":
        if isinstance(key, str): 
            if key in self._dict:
                return self._dict[key].all(**kwargs)
            else:
                raise KeyError
        else:
            raise TypeError
    
    def __len__(self):
        return len(self._dict)

    def __getitem__(self, key:str):
        if isinstance(key, str) and key in self._dict:
            return lambda kwargs: self._dict[key].all(**kwargs)
        else:
            raise KeyError