#! /bin/env python3

import io
import sys

def printer(stream, *args, **kwargs):
    if stream is not None:
        kw = {**kwargs, 'file': stream}
        print(*args, **kw)